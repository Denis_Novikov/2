// меню бургер
const burgerBtn = document.querySelector('.burger');
const menuClose = document.querySelector('.menu-close');
const menuBurger = document.querySelector('.nav_list');

burgerBtn.addEventListener( 'click' ,() => {
    menuBurger.classList.add('burger-active');
});

menuClose.addEventListener( 'click', () => {
    menuBurger.classList.remove('burger-active');
});

// Main
new Swiper('.swiper', {
  direction: 'horizontal',
  loop: true,
  
  pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
          return `<button class="btn btn_round ${className}" aria-label="Переключение слайда"></button>`
      }
  },
});
